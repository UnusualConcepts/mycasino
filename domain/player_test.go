package domain

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPlayer(t *testing.T) {
	t.Run("is not in game by default", func(t *testing.T) {
		player := createPlayer()

		assert.False(t, player.IsInGame())
	})

	t.Run("joins game IsInGame returns true", func(t *testing.T) {
		player := createPlayer()

		player.Join(createGame())

		assert.True(t, player.IsInGame())
	})
}

func createGame() *Game {
	return &Game{}
}

func createPlayer() *Player {
	return &Player{}
}
