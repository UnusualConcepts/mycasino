package domain

type Player struct {
	inGame bool
}

func (p *Player) IsInGame() bool {
	return p.inGame
}

func (p *Player) Join(game *Game) {
	p.inGame = true
}
